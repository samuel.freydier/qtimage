# Étape 1 : Utiliser une image de base pour la construction
FROM ubuntu:latest AS base

LABEL stage=qt-build-base

# UID/GID injection on build if wanted
ARG USER_UID=
ARG USER_GID=

ENV QT_USERNAME=qt
ENV QT_PREFIX=/usr/local

# Étape 2 : Installer les dépendances nécessaires pour la construction de Qt
RUN apt-get update && apt-get -y dist-upgrade && apt-get -y --no-install-recommends install \
	ca-certificates \
	# sudo to be able to modify the container as the user, if needed.
	sudo \
	curl \
	gperf \
	bison \
	flex \
	wget \
	build-essential \
	pkg-config \
	libgl1-mesa-dev \
	libicu-dev \
	firebird-dev \
	libmysqlclient-dev \
	libpq-dev \
	# bc suggested for openssl tests
	bc \
	libssl-dev \
	# git is needed to build openssl in older versions
	git \
	# xcb dependencies
	libfontconfig1-dev \
	libfreetype6-dev \
	libx11-dev \
	libxext-dev \
	libxfixes-dev \
	libxi-dev \
	libxrender-dev \
	libxcb1-dev \
	libx11-xcb-dev \
	libxcb-glx0-dev \
	libxkbcommon-x11-dev \
	libxcb-shm0-dev \
	libxcb-icccm4-dev \
	libxcb-image0-dev \
	libxcb-keysyms1-dev \
	libxcb-render-util0-dev \
	libxcb-xinerama0-dev \
	libxcb-util-dev \
	x11proto-record-dev \
	libxtst-dev \
	libatspi2.0-dev \
	libatk-bridge2.0-dev \
	# bash needed for argument substitution in entrypoint
	bash \
	# since 5.14.0 we apparently need libdbus-1-dev and libnss3-dev
	libnss3-dev \
	libdbus-1-dev \
	&& apt-get -qq clean \
	&& rm -rf /var/lib/apt/lists/* \
	&& printf "#!/bin/sh\nls -lah" > /usr/local/bin/ll && chmod +x /usr/local/bin/ll

# Adding regular user
RUN if [ ${USER_GID} ]; then \
	addgroup -g ${USER_GID} ${QT_USERNAME}; \
	else \
	addgroup ${QT_USERNAME}; \
	fi \
	&& if [ ${USER_UID} ]; then \
	useradd -u ${USER_UID} -g ${QT_USERNAME} ${QT_USERNAME}; \
	else \
	useradd -g ${QT_USERNAME} ${QT_USERNAME}; \
	fi && mkdir /home/${QT_USERNAME}

# make sure the user is able to sudo if needed
RUN adduser ${QT_USERNAME} sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

FROM base as builder

LABEL stage=qt-build-builder

# QT Version
ARG QT_VERSION_MAJOR=5
ARG QT_VERSION_MINOR=6
ARG QT_VERSION_PATCH=3

ENV QT_BUILD_ROOT=/tmp/qt_build
ENV QT_BUILD_DIR=$(QT_BUILD_ROOT)/qt-everywhere-opensource-src-${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}.${QT_VERSION_PATCH}/build

WORKDIR $(QT_BUILD_ROOT)

# Étape 3 : Télécharger et extraire les sources de Qt
RUN wget https://download.qt.io/new_archive/qt/${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}/${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}.${QT_VERSION_PATCH}/single/qt-everywhere-opensource-src-${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}.${QT_VERSION_PATCH}.tar.xz
RUN tar -xf qt-everywhere-opensource-src-${QT_VERSION_MAJOR}.${QT_VERSION_MINOR}.${QT_VERSION_PATCH}.tar.xz

WORKDIR $(QT_BUILD_DIR)

ARG CI_BUILD=
ENV CI_BUILD=$(CI_BUILD)

ARG CORE_COUNT=1
ENV CORE_COUNT=$(CORE_COUNT)

ADD buildconfig/configure.sh configure.sh
RUN mkdir /opt/extra-dependencies && chmod +x ./configure.sh && ./configure.sh $(CORE_COUNT) $(CI_BUILD)

COPY buildconfig/build.sh build.sh
RUN ./build.sh $(CI_BUILD) $(CORE_COUNT)

RUN make install

# resulting image with environment
FROM base as qt

ENV ENTRYPOINT_DIR=/usr/local/bin
ENV APP_BUILDDIR=/var/build

COPY --from=builder ${QT_PREFIX} ${QT_PREFIX}

RUN true
COPY --from=builder /opt/extra-dependencies /opt/extra-dependencies

#for modifications during configuration
ENV LD_LIBRARY_PATH=/opt/extra-dependencies/lib:${LD_LIBRARY_PATH}

RUN true
COPY entrypoint.sh ${ENTRYPOINT_DIR}

RUN chmod +x ${ENTRYPOINT_DIR}/entrypoint.sh

VOLUME ["${APP_BUILDDIR}"]

USER ${QT_USERNAME}

ENTRYPOINT ["entrypoint.sh"]